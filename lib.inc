section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov     rax, 60 
    xor     rdi, rdi
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r9, rsp
    push 0            ; end of line for stack output
    mov rdi, rsp      ; save the top of the stack for variables
    sub rsp, 20       ; we have a maximum of 19 variables, so we do -20
    mov r10, 10       ; divider
.loop:
    xor rdx, rdx      ; register rdx - a place to store the remainder of the division
    div r10           ; divide by 10
    add dl, 0x30      ; add 0x30 to the remainder. it'll be in 10 number system
    dec rdi           ; next number address
    mov [rdi], dl     ; save in memory
    test rax, rax
    jnz .loop
    
    call print_string
    mov rsp, r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte[rdi]
    cmp al, byte[rsi]
    jne .various
    inc rdi
    inc rsi
    test al, al
    jne string_equals
    mov rax, 1
    ret
.various:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r13
    push r14
    xor r14, r14
    mov r10, rsi
    mov r13, rdi
.loop:
    call read_char
    cmp al, 0x20       ; space check - space
    je .loop
    cmp al, 0x9        ; space check - tabulation
    je .loop
    cmp al, 0xA        ; space check - new line
    je .loop
    cmp al, 0x0        ; space check - zero byte
    je .end
    jmp .write
.read:
    call read_char
    cmp al, 0x0
    je .end
    cmp al, 0x20
    je .end
    cmp al, 0x9
	je .end	
	cmp al, 0xA
	je .end
    jmp .write
.write:
    mov byte [r13 + r14], al
    inc r14
    cmp r14, r10
    je .zeroing
    jmp .read
.end:
    mov rax, r13
    mov byte [r13 + r14], 0

    mov rdx, r14
    jmp .return
.zeroing:
    xor rax, rax
    xor rdx, rdx
.return:
    pop r14
    pop r13
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r8, 10
    xor rcx, rcx
    xor r9, r9
.loop:
    mov r9b, byte [rdi + rcx]
    cmp r9b, 0x30     ; 0x30 - "0" in 10 number system
    jb .end
    cmp r9b, 0x39     ; 0x39 - "9" in 10 number system
    ja .end
    mul r8
    sub r9b, 0x30
    add rax, r9
    inc rcx
    jmp .loop
    ret
.end:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    je .signed
    jmp parse_uint
.signed:
    inc rdi
    call parse_uint
    neg rax
    test rdx, rdx
    jz .error
    inc rdx
    ret
.error:
    xor rax, rax
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push r12
    xor rcx, rcx
.loop:
    cmp rcx, rdx
    je .zeroing
    mov r12, [rdi + rcx]
    mov [rsi, rcx], r12
    cmp r12, 0
    je .success
    inc rcx
    jmp .loop
.zeroing:
    xor rax, rax
    jmp .exit
.success:
    mov rax, rcx
    jmp .exit
.exit:
    pop r12
    ret
